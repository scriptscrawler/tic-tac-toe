

# Start the game, play One chooses X or O then you move on
def start_game():
    playerOneName = input('Please enter your name Player 1: ').strip()
    playerTwoName = input('Please enter your name Player 2: ').strip()
    playerOneSymbol = input(
        'Player 1 please choose your symbol(X or O): ').strip()
    playerTwoSymbol = ''
    if playerOneSymbol == 'x' or playerOneSymbol == 'X':
        playerOneSymbol = 'X'
        playerTwoSymbol = 'O'
    elif playerOneSymbol == 'o' or playerOneSymbol == 'O':
        playerOneSymbol = 'O'
        playerTwoSymbol = 'X'
    else:
        playerOneSymbol = input('You must Enter an X or an O: ')
    return {"currentPlayerTurn": 1, "playerOneSymbol": playerOneSymbol, "playerTwoSymbol": playerTwoSymbol, "playerOneName": playerOneName, "playerTwoName": playerTwoName}


# Generate the board onthe screen with placeholders for the numbers
def generate_board(board):
    print('          ')
    print(f'{board[0]} | {board[1]} | {board[2]}')
    print('----------')
    print(f'{board[3]} | {board[4]} | {board[5]}')
    print('----------')
    print(f'{board[6]} | {board[7]} | {board[8]}')
    print('          ')


# Accept input from either user and then apply the correct symbol
def user_input(player, board):
    numberSystem = {
        7: 0,
        8: 1,
        9: 2,
        4: 3,
        5: 4,
        6: 5,
        1: 6,
        2: 7,
        3: 8
    }
    playerInput = input(
        f'Please choose your position to play Player {player}: ')
    if [' ', 1, 2, 3, 4, 5, 6, 7, 8, 9].index(int(playerInput)):
        if player == 1:
            if board[numberSystem[int(playerInput)]] == ' ':
                # print(f'Lookie here {numberSystem[int(playerInput)]}')
                board[numberSystem[int(playerInput)]
                      ] = user_info["playerOneSymbol"]
                user_info["currentPlayerTurn"] = 2
            else:
                user_input(user_info["currentPlayerTurn"], board)
        else:
            if board[numberSystem[int(playerInput)]] == ' ':
                board[numberSystem[int(playerInput)]
                      ] = user_info["playerTwoSymbol"]
                user_info["currentPlayerTurn"] = 1
            else:
                user_input(user_info["currentPlayerTurn"], board)

    # print(board)
    generate_board(board)
    do_we_have_a_win(board)


# We need to check if their has been a win
def do_we_have_a_win(board):
    if board[0] + board[1] + board[2] == 'XXX' or board[0] + board[1] + board[2] == 'OOO':
        if board[0] + board[1] + board[2] == 'XXX':
            if user_info["playerOneSymbol"] == 'X':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
        else:
            if user_info["playerOneSymbol"] == 'O':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
    elif board[3] + board[4] + board[5] == 'XXX' or board[3] + board[4] + board[5] == 'OOO':
        if board[3] + board[4] + board[5] == 'XXX':
            if user_info["playerOneSymbol"] == 'X':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
        else:
            if user_info["playerOneSymbol"] == 'O':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
    elif board[6] + board[7] + board[8] == 'XXX' or board[6] + board[7] + board[8] == 'OOO':
        if board[6] + board[7] + board[8] == 'XXX':
            if user_info["playerOneSymbol"] == 'X':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
        else:
            if user_info["playerOneSymbol"] == 'O':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
    elif board[0] + board[3] + board[6] == 'XXX' or board[0] + board[3] + board[6] == 'OOO':
        if board[0] + board[3] + board[6] == 'XXX':
            if user_info["playerOneSymbol"] == 'X':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
        else:
            if user_info["playerOneSymbol"] == 'O':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
    elif board[1] + board[4] + board[7] == 'XXX' or board[1] + board[4] + board[7] == 'OOO':
        if board[1] + board[4] + board[7] == 'XXX':
            if user_info["playerOneSymbol"] == 'X':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
        else:
            if user_info["playerOneSymbol"] == 'O':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
    elif board[2] + board[5] + board[8] == 'XXX' or board[2] + board[5] + board[8] == 'OOO':
        if board[2] + board[5] + board[8] == 'XXX':
            if user_info["playerOneSymbol"] == 'X':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
        else:
            if user_info["playerOneSymbol"] == 'O':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
    elif board[6] + board[4] + board[2] == 'XXX' or board[6] + board[4] + board[2] == 'OOO':
        if board[6] + board[4] + board[2] == 'XXX':
            if user_info["playerOneSymbol"] == 'X':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
        else:
            if user_info["playerOneSymbol"] == 'O':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
    elif board[0] + board[4] + board[8] == 'XXX' or board[0] + board[4] + board[8] == 'OOO':
        if board[0] + board[4] + board[8] == 'XXX':
            if user_info["playerOneSymbol"] == 'X':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
        else:
            if user_info["playerOneSymbol"] == 'O':
                print('Player One has won')
            else:
                print('Player Two Has Won')
            return
    else:
        user_input(user_info["currentPlayerTurn"], board)
# Game Play


user_info = start_game()
game_board = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']

generate_board(game_board)
user_input(user_info["currentPlayerTurn"], game_board)
